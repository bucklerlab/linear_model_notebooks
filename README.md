# README #

### Purpose of Linear_Model_Notebooks ###

* This repository holds R (.Rmd) and Jupyter notebooks (.pynb) that use third party software for solving linear models.
* rTASSEL and rPHG are used as data sources and for data management.
* Each notebook should contain the necessary steps and instructions for running an analysis.
* Each notebook uses toy data sets to demonstrate but should be easy to modify to run analysis with full scale data.

### Who do I talk to? ###

* Peter Bradbury
